// let search = "dog";
const api_key = '75490dc30d2f6e41e2cdd4e36cc5b927';
let image = []
let search = prompt("Search photo...");

fetch(`https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=${api_key}&tags=${search}&safe_search=2&per_page=50&in_gallery=&format=json&nojsoncallback=1`)
    .then(res => res.json())
    .then(function (data) {
        // console.log(data);
        let result = data.photos.photo;
        // console.log('result:', result)
        result.forEach(photo => {
            // console.log(photo.title);
            let url = `https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}_q.jpg`;
            image.push(url)
        });
        console.log(image);
        let {
            nextBtn,
            img
        } = createImg();
        let i = 0
        i = nextButton(nextBtn, img, i);
    });

function nextButton(nextBtn, img, i) {
    nextBtn.addEventListener('click', function () {
        img.src = image[i];
        if (i < image.length - 1) {
            i++;
        } else {
            i = 0;
        }
    });
    return i;
}

function createImg() {
    let num = Math.floor(Math.random() * image.length);
    let nextBtn = document.getElementById('next');
    let src = document.getElementById("photos");
    let img = document.createElement("img");
    img.src = image[num];
    src.appendChild(img);
    return {
        nextBtn,
        img
    };
}